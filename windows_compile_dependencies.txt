Things I had to install (or edit PATH environment variable) for Windows compilation

MinGW 
	Linux command line operations, sed, etc
Msys
	Allows for make
Ghostscript 
	combining pdfs
	%needed to rename the gswin64 executable to 'gs'
ImageMagick
	converting dot png's to correct size
	%Had to directly path to the ImageMagick convert tool
Python 2.7
	for all the scripts
GraphViz
	to create the graphs, 'dot' command
Inkscape
	svg to pdf conversion

Also: Dropbox tries to sync sed's temp files and can't catch up. Makes sed think it has permission errors.