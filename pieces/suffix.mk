top_dir     ?= .
src_dir      ?= $(top_dir)/src
graph_dir   ?= $(top_dir)/graphs
png_dir     ?= $(graph_dir)/png
dot_dir     ?= $(graph_dir)/dot
card_dir    ?= $(top_dir)/cards
width ?= 2.75
height ?= 2
dpi ?= 300

name ?= $(lastword $(subst /, ,$(abspath $(top_dir))))

graphs := $(notdir $(basename $(wildcard $(src_dir)/joe/*.joe)))

Epngs := $(addsuffix E.png, $(graphs))

Fpngs := $(addsuffix F.png, $(graphs))

all: Ecards Fcards

%E.png: $(src_dir)/joe/$(notdir %).joe
	mkdir -p $(dot_dir)
	mkdir -p $(png_dir)
	../scripts/joe2image.py -i $(src_dir)/joe/$(notdir $(basename $<)).joe -o $(png_dir) -d $(dot_dir) -m 6 --ext=png --dpi=$(dpi) --width=$(width) --height=$(height)

%F.png: $(src_dir)/joe/$(notdir %).joe
	mkdir -p $(dot_dir)
	mkdir -p $(png_dir)
	../scripts/joe2image.py -i $(src_dir)/joe/$(notdir $(basename $<)).joe -o $(png_dir) -d $(dot_dir) -m 6 --ext=png --dpi=$(dpi) --width=$(width) --height=$(height) -f

Ecards: $(Epngs)
	mkdir -p $(card_dir)
	../scripts/csv2svg.py -i $(src_dir)/$(name).csv -t $(src_dir)/$(name)-template-small.svg -o $(card_dir) --dpi=$(dpi) -s $(name)
	rm $(card_dir)/*.svg.bak

Fcards: $(Fpngs)
	mkdir -p $(card_dir)
	../scripts/csv2svg.py -i $(src_dir)/$(name).csv -t $(src_dir)/$(name)-template-small.svg -o $(card_dir) --dpi=$(dpi) -s $(name) -f
	rm $(card_dir)/*.svg.bak

clean:
	-rm -rf $(graph_dir) $(card_dir)

.PHONY: all cards clean
