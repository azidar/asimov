top_dir     ?= .
src_dir     ?= $(top_dir)/src
card_dir    ?= $(top_dir)/cards

name ?= $(lastword $(subst /, ,$(abspath $(top_dir))))

all: Ecards Fcards

Ecards: 
	mkdir -p $(card_dir)
	../scripts/csv2svg.py -i $(src_dir)/$(name).csv -t $(src_dir)/$(name)-template-small.svg -o $(card_dir) -s $(name)
	rm $(card_dir)/*.svg.bak

Fcards: 
	mkdir -p $(card_dir)
	../scripts/csv2svg.py -i $(src_dir)/$(name).csv -t $(src_dir)/$(name)-template-small.svg -o $(card_dir) -s $(name) -f
	rm $(card_dir)/*.svg.bak

clean:
	-rm -rf $(card_dir)

.PHONY: all cards clean
