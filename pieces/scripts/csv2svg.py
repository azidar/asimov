#!/usr/bin/env python

import sys 
import os
import shutil
import csv
from optparse import OptionParser
from subprocess import call
from util import *

def getGraphPath(name,options):
	#if(options.graphDir == "."):
	#	return name
	#else:
	#	return options.graphDir+"/"+name
	if(not options.friendly):
		#return "href=\"../graphs/png/"+name+"E\.png\""
		return "../graphs/png/"+name+"E\.png"
	else:
		#return "href=\"../graphs/png/"+name+"F\.png\""
		return "../graphs/png/"+name+"F\.png"
		

def getTemplateName(options):
	return options.template

def getOutputName(name,options):
	if(options.friendly):
		return options.outDir+"/"+name+"F.svg"
	else:
		return options.outDir+"/"+name+"E.svg"		

def findReplace(out,replacements):
	print replacements
	for k,v in replacements.iteritems():
                print k
                print v
                if(not (v is None)):
		  s = "s/%s/%s/g" % (k.replace("/","\/"),v.replace("/","\/"))
		  print "sed -i.bak", s, out
		  call(["sed", "-i.bak",s, out])

def buildReplacements(row,options):
	replacements = {}	

	replacements["xxName"]=row['Name']
	replacements["xxType"]=row['Type']
	replacements["deaded"]=row['IconColor']
	replacements["xxIcon\.png"]= row['IconFile'] + "\.png"
	replacements["absref"]="absref"
	teal = "0097ff"
	white = "ffffff"
	black = "000000"
	if(options.system=="shield"):
		replacements["xxGraph\.png"]= getGraphPath(row['Graph'],options)
		#replacements["href=\\\"xxGraph.png\\\""]= getGraphPath(row['Graph'],options)
		loc = row['Direction']

		if('f' in loc):
			replacements["1deadb"] = teal
		else:
			replacements["1deadb"] = white
		if('l' in loc):
			replacements["2deadb"] = teal
		else:
			replacements["2deadb"] = white
		if('b' in loc):
			replacements["3deadb"] = teal
		else: 
			replacements["3deadb"] = white
		if('r' in loc):
			replacements["4deadb"] = teal
		else: 
			replacements["4deadb"] = white

	elif(options.system=="weapon"):
		replacements["xxGraph\.png"]= getGraphPath(row['Graph'],options)
		#replacements["href=\\\"xxGraph.png\\\""]= getGraphPath(row['Graph'],options)
		replacements["xxSh1"]=row['Shield1']
		replacements["xxAr1"]=row['Armor1']
		replacements["xxEx1"]=row['Explosion1']
		replacements["xxRa1"]=row['Range1']
		replacements["xxAn1"]=row['Angle1']
		replacements["xxAc1"]=row['Accuracy1']
		replacements["xxSh2"]=row['Shield2']
		replacements["xxAr2"]=row['Armor2']
		replacements["xxEx2"]=row['Explosion2']
		replacements["xxRa2"]=row['Range2']
		replacements["xxAn2"]=row['Angle2']
		replacements["xxAc2"]=row['Accuracy2']
		if(row['Shield2']==""):
			replacements["1deadb"] = white
		else:
			replacements["#1deadb"] = "none"

	elif(options.system=="missile"):
		replacements["xxSh"]=row['Shield']
		replacements["xxAr"]=row['Armor']
		replacements["xxEx"]=row['Explosion']
		replacements["xxSp"]=row['Speed']
		replacements["xxTu"]=row['Turn']
		replacements["xxAc"]=row['Accuracy']
		replacements["xxDi"]=row['Diameter']
		replacements["xxDescription"]=row['Description']
	
	elif(options.system=="controller"):
		replacements["xxNumber"]=row['Number']
		replacements["xxEnergy"]=row['Energy']

	elif(options.system=="thruster"):
		m =1
		while(m<=3):
			t = 1
			while(t<=3):
				replacements["xxM%dT%d"%(m,t)]=row['M%dT%d'%(m,t)]
				replacements["xxM%dE%d"%(m,t)]=row['M%dE%d'%(m,t)]
				t = t+1
			m = m+1

		m = 1
		dirs = "fxlbrc"
		while(m<=3):
			i = 1
			for char in dirs:
				d = row['M%dDirection'%m].find(char)
				if(d==-1):
					replacements["%d%ddead"%(m,i)] = white
				else:
					replacements["%d%ddead"%(m,i)] = black
				i = i+1
			m = m+1
			
	elif(options.system=="scanner"):
		replacements["xxScans"]=row['Scans']
		m = 1
		while(m<=3):
			replacements["xxS%dTarget"%m]=row['S%dTarget'%m]
			replacements["xxS%dEnergy"%m]=row['S%dEnergy'%m]
			if(row['S%dOdds'%m]!=""):
				replacements["xxS%dOdds"%m]=str(int(float(row['S%dOdds'%m])*6)+1)+"/6"
			else:
				replacements["xxS%dOdds"%m]=""
			m = m +1

	elif(options.system=="launcher"):
		replacements["xxDiameter1"]=row['Diameter1'] + " cm"
		replacements["xxEnergy1"]=row['Energy1']
		replacements["xxDiameter2"]=row['Diameter2'] + " cm"
		replacements["xxEnergy2"]=row['Energy2']
		replacements["xxGraph\.png"]=getGraphPath(row['Graph'],options)
		replacements["xxGraph2\.png"]=getGraphPath(row['Graph2'],options)

	elif(options.system=="repair"):
		replacements["xxGraph\.png"]= getGraphPath(row['Graph'],options)
		#replacements["href=\\\"xxGraph.png\\\""]= getGraphPath(row['Graph'],options)

	return replacements

def convertCsv2Svg(options):	
	inFile = open(options.csv, 'r')
	template = getTemplateName(options)

	with open(options.csv) as csvfile:
		reader = csv.DictReader(csvfile)
		for row in reader:
			replacements = buildReplacements(row,options)
			out = getOutputName(row['Name'],options)
			shutil.copyfile(template, out)
			findReplace(out,replacements)
			call(["inkscape", out,"-e"+out.replace("svg","png"), "-d %d"%options.dpi,"-z"])
	
	inFile.close()

def parseCommandLine():
	parser = OptionParser()	
	parser.add_option("-i", "--input", dest="csv",help="input csv file.", metavar="FILE", default = "")
	parser.add_option("-g", "--graphs", dest="graphDir",help="input directory of png graphs", metavar="DIR", default="")
	parser.add_option("-t", "--template", dest="template",help="input svg template", metavar="FILE", default="")
	parser.add_option("-o", "--output", dest="outDir",help="output directory for all svg cards from csv", metavar="DIR", default="")
	parser.add_option("--dpi", type="int",dest="dpi",help="sets output png dpi", metavar="INT", default="300")
	parser.add_option("-s", "--system", dest="system",help="system to generate. Either shield, weapon, repair, scanner, thruster, missile, launcher, controller", metavar="SYSTEM", default="")
	parser.add_option("-v", "--verbose",action="store_false", dest="verbose", default=True, help="don't print status messages to stdout")
	parser.add_option("-f", "--friendly",action="store_true", dest="friendly", default=False, help="generate for friendly system")
	(options, args) = parser.parse_args()


	if(options.csv==""):
		sys.exit('Error! No input csv file provided')
	if(options.template==""):
		sys.exit('Error! No input template file provided')

	if(options.outDir==""):
		end=options.csv.rfind("/")
		options.outDir = options.csv[0:end]
		if(end == -1):
			options.outDir = "."

	return options

if __name__ == "__main__":
	options = parseCommandLine()
	convertCsv2Svg(options)


