def getFile(fileStr):
	start = fileStr.rfind("/")
	if(start==-1):
		return fileStr	
	return fileStr[start+1:]

def getDir(fileStr):
	start = fileStr.rfind("/")
	if(start==-1):
		return "./"	
	return fileStr[:start-1]
