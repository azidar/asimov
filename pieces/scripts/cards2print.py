#!/usr/bin/env python

import sys 
import shutil
import csv
import posixpath
from optparse import OptionParser
from subprocess import call
from util import *
import os

def findReplace(out,replacements):
	print replacements
	for k,v in replacements.iteritems():
		s = "s/%s/%s/g" % (k.replace("/","\/"),v.replace("/","\/"))
		print "sed -i.bak", s, out
		call(["sed", "-i.bak",s, out])

def getTemplateName(options):
	return options.template

def addzeros(i,n):
  if(len(str(i)) < n):
    return str(0)*(n-len(str(i))) + str(i)
  else:
    return str(i)

def getOutputName(i,options):
	return options.outDir+"/"+addzeros(i,2)+".svg"

def buildReplacements(files, i,options):
	replacements = {}	

	#replacements["href=\\\"xxCard.png\\\""]= "href=\"../graphs/png/"+name+"E\.png\""
	j = 0
	while(i+j<len(files) and j<options.perPage):
		print("Building card print replacements")
		print(files[i+j])
		replacements["../src/xxCard"+str(j)+".png"]=files[i+j]
		j = j+1

	print replacements
	return replacements


def convertCards2Print(options):	
	template = getTemplateName(options)
	Efiles = []
	Ffiles = []

	for d in options.dirs:
		for f in os.listdir(d):
			print f
			if(f.find("E.png")!=-1):
				OSpath = os.path.abspath(d)+"/"+f
				Efiles.append(OSpath.replace('\\','/'))				
			if(f.find("F.png")!=-1):
				OSpath = os.path.abspath(d)+"/"+f
				Ffiles.append(OSpath.replace('\\','/'))				
				#Cannot use a \ Windows filepath with sed. sed uses it to escape options
				#This led to some rather hilarious looking filepaths on my machine

	i = 0
	while (i<len(Efiles)):
		out = getOutputName(i,options)
		shutil.copyfile(template, out)
		replacements = buildReplacements(Efiles,i,options)
		findReplace(out,replacements)
		i = i+options.perPage
		call(["inkscape", out,"-e"+out.replace("svg","png"), "-d %d"%options.dpi, "-z"])
		call(["inkscape", out,"-A"+out.replace("svg","pdf"), "-d %d"%options.dpi, "-z"])
        j = 0
	while (j<len(Ffiles)):
		out = getOutputName(i+j,options)
		shutil.copyfile(template, out)
		replacements = buildReplacements(Ffiles,j,options)
		findReplace(out,replacements)
		j = j+options.perPage
		call(["inkscape", out,"-e"+out.replace("svg","png"), "-d %d"%options.dpi, "-z"])
		call(["inkscape", out,"-A"+out.replace("svg","pdf"), "-d %d"%options.dpi, "-z"])

def parseCommandLine():
	parser = OptionParser()	
	parser.add_option("-i", "--input", action="append",dest="dirs",help="dirs to print.  Can be used multiple times.", metavar="DIR", default = [])
	parser.add_option("-t", "--template", dest="template",help="input svg template", metavar="FILE", default="")
	parser.add_option("-p", "--perPage",type="int",dest="perPage",help="sets number of cards per page on template", metavar="INT", default=5)
	parser.add_option("-o", "--output", dest="outDir",help="output directory for all svg cards", metavar="DIR", default="")
	parser.add_option("--dpi", type="int",dest="dpi",help="sets output png dpi", metavar="INT", default="300")
	(options, args) = parser.parse_args()

	if(options.template==""):
		sys.exit('Error! No input template file provided')
	if(options.dirs==[]):
		sys.exit('Error! No input dirs provided')
	if(options.outDir==""):
		sys.exit('Error! No output dir provided')

	return options

if __name__ == "__main__":
	options = parseCommandLine()
	convertCards2Print(options)
	


