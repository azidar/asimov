#!/usr/bin/env python

import sys
import os 
from optparse import OptionParser
from subprocess import call
from util import *

def dotHeaderString(options):
	s = ""
	s +=  "digraph{\n"
	#s += "\tsize= \"5,5\";\n"
	s += "\tsplines=curve;\n"
	s += "\toverlap = false;\n"
	s += "\tlabelfloat=false;\n"
	s += "\tsep=1;\n"
	s += "\tnodesep=0.6;\n"
	s += "\tnode [nodesep=2.0, fontsize=20];\n"
	#s += "\tedge [fontcolor=\"darkorange2\",  fontname=\"Times-Bold\", fontsize=26, decorate=true, weight=0, forcelabel=true];\n"
	s += "\tedge [fontcolor=\"darkorange2\",  fontname=\"Times-Bold\", fontsize=26, decorate=false, weight=0, forcelabel=true];\n"
	s += "\trankdir=\"LR\";\n"
	s += "\tmodel=\"circuit\";\n"
	s += "OFF [pos=\"0,0\"]"
	#s += "\tratio=\"fill\";\n"
	s += "\n\n"
	return s

def convertJoe2Dot(options):	
	print options.dotFile
	inFile = open(options.inFile, 'r')	
	dotFile = open(options.dotFile, 'w')

	dotFile.write(dotHeaderString(options))

	for line in inFile.readlines():
		label = ""

		#if(line.find("OFF")!=line.rfind("OFF")):
			#line = "OFF:nw->OFF:sw"+line[line.rfind("OFF")+3:]

		if("-->" in line):
			arrow = line.find("-->")
			end = min(line.find("["), line.find(";"))
			node1 = line[:arrow].strip()
			node2 = line[arrow+3:end].strip()
			rest = line[end:].strip()+"\n"
			line = node2 + " -> "+ node1+" [dir=back] "+rest

		if(options.friendly and "->" in line):
			start = line.rfind("e=")+2
			end = line.find(",")
			if(end<0):
				end=line.__len__()-1
			label = line[start:end].strip()
			fullLabel = "[label=\"  %s\"];" % label
			line = line.replace(";",fullLabel)
			dotFile.write("\t"+line)
		elif("->" in line):
			start = line.rfind("[")+1
			end = line.rfind("]",start)
			if(end<0):
				end=line.__len__()-1
			label = line[start:end].strip()
			if(label!=""):
				comma = label.rfind(",")
				lo = int(float(label[0:comma])*options.max)+1
				hi = int(float(label[comma+1:])*options.max)
				fullLabel = "[label=\"  %d-%d\"];" % (lo,hi)
			else:
				fullLabel = "[label=\"  %s\"];" % ""
			line1 = line.replace(";",fullLabel)
			if(not "OFF" in line):
				dotFile.write("\t"+line1)
			elif(line.find("OFF")<line.find("->") and line.find("OFF")==line.rfind("OFF")):
				dotFile.write("\t"+line)
		else:
			dotFile.write("\t"+line)

		
	
	# close bracket from header
	dotFile.write("}")

	inFile.close()
	dotFile.close()

def parseCommandLine():
	parser = OptionParser()	
	parser.add_option("-i", "--input", dest="inFile",help="convert this .joe file", metavar="FILE", default = "")
	parser.add_option("-o", "--output", dest="outDir",help="output to image directory, defaults to same name as input", metavar="DIR", default="")
	parser.add_option("-d", "--dot", dest="dotDir",help="output to dot directory, defaults to same name as input", metavar="DIR", default="")
	parser.add_option("-m", "--maxDie", type="int",dest="max",help="set maximum die value for enemy", metavar="INT", default="6")
	parser.add_option("-v", "--verbose",action="store_false", dest="verbose", default=True, help="don't print status messages to stdout")
	parser.add_option("--width", type="float",dest="width",help="sets image width in inches", metavar="INT", default="4.5")
	parser.add_option("--height", type="float",dest="height",help="sets image height in inches", metavar="INT", default="3")
	parser.add_option("--dpi", type="int",dest="dpi",help="sets image dpi", metavar="INT", default="300")
	parser.add_option("-f", "--friendly",action="store_true", dest="friendly", default=False, help="generate for friendly system")
	parser.add_option("--ext",dest="ext", default="png", help="image extension, either svg or png")
	(options, args) = parser.parse_args()	

	options.dotFile = ""
	options.outFile=""

	if(options.inFile==""):
		sys.exit('Error! No input file provided')

	if(options.dotDir==""):
		options.dotDir = getDir(options.inFile)
	options.dotFile=options.dotDir + "/" +getFile(options.inFile).replace(".joe",".dot")
	if(options.friendly):
		options.dotFile=options.dotFile.replace("."+options.ext,"F."+options.ext)
	else:
		options.dotFile=options.dotFile.replace("."+options.ext,"E."+options.ext)

	if(options.dotDir==""):
		options.outDir = getDir(options.inFile)
	options.outFile=options.outDir + "/" +getFile(options.inFile).replace(".joe","."+options.ext)
	if(options.friendly):
		options.outFile=options.outFile.replace("."+options.ext,"F."+options.ext)
	else:
		options.outFile=options.outFile.replace("."+options.ext,"E."+options.ext)

	options.max = int(options.max)
	return options

def convertDot2Image(options):
	call(["dot", "-T"+options.ext,"-Gsize=%d,%d!"%(options.height,options.width), "-Gdpi=%d"%options.dpi, options.dotFile, "-o" +options.outFile])

def resizeImage(options):
		w = options.height*options.dpi
		h = options.width*options.dpi
		if (sys.platform.find('win') != 0):
			convertCommand = "convert"
		else:
			convertCommand = "C:\Program Files\ImageMagick-6.9.0-Q16\convert.exe"
		#call(["convert", options.outFile,"-gravity", "center", "-resize", "%dx%d"%(h,w), "-extent", "%dx%d"%(h,w),options.outFile])
		call([convertCommand, options.outFile, "-gravity", "center", "-resize", "%dx%d"%(h,w), "-extent", "%dx%d"%(h,w), options.outFile])

if __name__ == "__main__":
	options = parseCommandLine()
	convertJoe2Dot(options)
	convertDot2Image(options)
	if(options.ext!="svg"):
		resizeImage(options)
